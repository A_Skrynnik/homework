let name;
let age;
let confirmQuestion;
let defaultName = '';
let defaultAge = '';

name = prompt('Enter your name', '');
age = prompt('Enter your age', '');


//Дополнительное задание: проверка данных на коректность
while (name === ''|| isNaN(age) ) {
    defaultName = name;
    defaultAge = age;
    name = prompt('Enter your name', defaultName);
    age = prompt('Enter your age', defaultAge);
}

if (age < 18) {
    alert('You are not allowed to visit this website.');
} else if (age > 18 && age < 22) {
    confirmQuestion = confirm('Are you sure you want to continue?');
    if (confirmQuestion){
        alert(`Welcome, ${name}`);
    }else {
        alert('You are not allowed to visit this website.');
    }
} else {
    alert(`Welcome, ${name}`);
}