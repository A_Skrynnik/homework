// Реализовать функцию, которая будет производить математические операции с введеными пользователем
// числами. Задача должна быть реализована на языке javascript, без использования
// фреймворков и сторонник библиотек (типа Jquery).
//
// Технические требования:
//
//     Считать с помощью модального окна браузера два числа.
//     Считать с помощью модального окна браузера математическую операцию,
//     которую нужно совершить. Сюда может быть введено +, -, *, /.
//
//     Создать функцию, в которую передать два значения и операцию.
//     Вывести в консоль результат выполнения функции.
//
//
//     Необязательное задание продвинутой сложности:
//
//     После ввода данных добавить проверку их корректности. Если пользователь не ввел числа,
//     либо при вводе указал не числа, - спросить оба числа заново
//     (при этом значением по умолчанию для каждой из переменных должна
//     быть введенная ранее информация).

let firstNumber = prompt('Введите первое число', '');
let secondNumber = prompt('Введите второе число', '');
let operationSign = prompt('Введите знак нужной операции', '');
let firstNumberDef = '';
let secondNumberDef = '';
let operationSignDef = '';
let res = '';

// Проверка данных на коректность
while (firstNumber === '' || secondNumber === '' || isNaN(firstNumber) || isNaN(secondNumber)) {
    firstNumberDef = firstNumber;
    secondNumberDef = secondNumber;
    operationSignDef = operationSign;
    firstNumber = prompt('Введите первое число', firstNumberDef);
    secondNumber = prompt('Введите второе число', secondNumberDef);
    operationSign = prompt('Введите знак нужной операции', operationSignDef);
}

function calc (firstNumber, secondNumber, operationSign){
    switch (operationSign) {
        case '+': res = Number(firstNumber) + Number(secondNumber);
        break;
        case '-': res = firstNumber - secondNumber;
        break;
        case '*': res = firstNumber * secondNumber;
        break;
        case '/': res = firstNumber / secondNumber;
        break;
    }
    return console.log(res);
}

calc(firstNumber, secondNumber, operationSign);