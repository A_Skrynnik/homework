function createNewUser () {
    const FIRST_NAME = prompt('Введите имя', '');

    const SECOND_NAME = prompt('Введите фамилию', '');

    const BIRTHDAY = prompt('Введите дату рождения в формате dd.mm.yyyy', '');

    const birthTimestamp = BIRTHDAY.split('.');

    let birthday = Date.parse(`${birthTimestamp[2]}-${birthTimestamp[1]}-${birthTimestamp[0]}`);
    const currentDate = Date.now();

    return {
        firstName: FIRST_NAME,
        secondName: SECOND_NAME,
        getLogin() {
            return this.firstName[0].toLowerCase() + this.secondName.toLowerCase();
        },
        getAge() {
            return new Date(currentDate - birthday).getFullYear() - 1970;
        },
        getPassword() {
            return this.firstName[0].toUpperCase() + this.secondName.toLowerCase() + birthTimestamp[2];
        }
    };
}


const newUser = createNewUser();
console.log(newUser)
console.log(newUser.getAge())
console.log(newUser.getPassword())