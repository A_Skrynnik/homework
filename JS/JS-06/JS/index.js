const filterBy = (arr, type) => {
    return arr.filter( item => typeof(item) !== type )
}

const arr = ['hello', 'world', 23, '23', null]

console.log(filterBy(arr, 'string'))
