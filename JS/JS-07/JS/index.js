const insertList = (arr, place = document.body) => {
    const items = arr.map( el => `<li>${el}</li>`)
    const list = document.createElement("ul")
    items.forEach( el => {list.innerHTML += `${el}`} )
    place.prepend(list)
}

insertList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"])