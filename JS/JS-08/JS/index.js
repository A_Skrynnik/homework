const input = document.createElement('input');
const label = document.createElement('label');
const currentPrice = document.createElement('span');
const wrongPrice = document.createElement('span');
const deleteBtn = document.createElement('button');
input.type = number;
wrongPrice.classList.add('price');
deleteBtn.innerText = 'X';
deleteBtn.classList.add('delete-btn')
currentPrice.classList.add('price');
label.textContent = 'Price: ';
label.append(input);
input.style.marginLeft = '5px';
document.body.append(label);

input.addEventListener('focus', element => {
    element.target.classList.add('active')
})


input.addEventListener('blur', element => {
    element.target.classList.remove('active')
    const value = element.target.value;
    if (+value > 0) {
        input.classList.add('valid-number')
        currentPrice.innerHTML = `Текущая цена: ${value}`
        currentPrice.append(deleteBtn);
        label.before(currentPrice)
        wrongPrice.remove();
    } else {
        input.classList.add('invalid-number');
        wrongPrice.innerHTML = `Please enter correct price`
        wrongPrice.append(deleteBtn);
        label.after(wrongPrice)
        currentPrice.remove();
    }
})

deleteBtn.addEventListener('click', element => {
    element.target.parentNode.remove();
    input.value = '';
    input.className = '';
})