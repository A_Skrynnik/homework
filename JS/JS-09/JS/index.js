const list = document.querySelector('.tabs'),
    listChildren = Array.from(list.children),
    tabs = document.querySelector('.tabs-content');


list.addEventListener('click', event => {

    if (event.target.classList.contains('tabs-title')){

        listChildren
            .forEach(el => {
            el.classList.remove('active')
        })

        event.target.classList.add('active')

        const activeIndex = listChildren
            .findIndex(el => el.classList.contains('active'))

        Array.from(tabs.children).forEach( (el, index) => {
                if (index !== activeIndex) {
                    el.style.display = 'none'
                } else el.style.display = ''
            }
        )
    }

})
