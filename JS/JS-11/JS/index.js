const buttons = Array.from(document.querySelector('.btn-wrapper').children)

document.addEventListener('keyup', event => {
    buttons.forEach(el => {
        if (el.textContent === event.code.slice(-1) ||
            el.textContent === event.code) {
            const active = document.querySelector('.btn-blue');
            console.log(active)
            if (active !== null) {
                active.classList.remove('btn-blue')
            }
            el.classList.add('btn-blue');
        }
    })
})