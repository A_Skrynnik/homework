const images = document.querySelectorAll('.image-to-show');
const div = document.createElement('div');
div.classList.add('button-wrapper')
const stop = `<button class='button stop'>Прекратить</button>`;
const resume = `<button class='button resume'>Возобновить показ</button>`;
div.insertAdjacentHTML("beforeend", stop);
div.insertAdjacentHTML("beforeend", resume);
document.body.append(div);
let currentIndex = 0;
let timerId = null;
const slideShow = () => {
timerId = setInterval(() => {
    if (currentIndex < images.length-1) {
        images[currentIndex].classList.add('hidden');
        images[++currentIndex].classList.remove('hidden');
    } else {
            images[images.length-1].classList.add('hidden');
            images[0].classList.remove('hidden');
            currentIndex = 0;
    }
}, 3000);
};

slideShow();

const stopBtn = document.querySelector('.stop');
const resumeBtn = document.querySelector('.resume');

stopBtn.addEventListener('click', () => {
    clearInterval(timerId);
});

resumeBtn.addEventListener('click', () => {
    slideShow()
});