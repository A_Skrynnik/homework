const btn = document.querySelector('.change-theme');
const body = document.querySelector('body');

if (localStorage.getItem('bodyStyle')) {
body.className = localStorage.getItem('bodyStyle');
}

btn.addEventListener('click', () => {
    body.classList.toggle('dark-theme');
    localStorage.setItem('bodyStyle', body.classList.value)
})