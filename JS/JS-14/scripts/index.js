$('.page-anchors-list').click( e => {
    if ( e.target !== e.currentTarget ) {
        e.preventDefault();
        $('html,body').stop().animate({ scrollTop: $(`.${e.target.parentElement.dataset.sectionName}`).offset().top }, 1000);

    };
})

$(window).scroll(function(){
    if($(window).scrollTop() > document.documentElement.clientHeight) {
        $('#scroll_top').show();
    } else {
        $('#scroll_top').hide();
    }
});

$('#scroll_top').click(function(){
    $('html, body').animate({scrollTop: 0}, 1000);
    return false;
});

$('.content_toggle').click(function(){
    $('.most-popular-posts-content').slideToggle(300, function () {
        if ($(this).is(':hidden')) {
            $('.content_toggle').text('Показать');
        } else {
            $('.content_toggle').text('Скрыть');
        }							
    });      
    return false;
});