let number = prompt('Enter number', '')
let incorrect = null;
let res = 0;

while (typeof +number !== 'number' || isNaN(+number)) {
    incorrect = number
    number = prompt('Enter number', incorrect)
}

const factorial = number => {
    return (number !== 1) ? number * factorial(number-1) : 1
}

console.log(factorial(number))