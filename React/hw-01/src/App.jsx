import React, { Component } from 'react';
import Button from './components/Button/Button';
import Modal from './components/Modal/Modal';
import styles from './App.module.scss';

class App extends Component {
  state = {
    isFirstOpen: false,
    isSecondOpen: false,
  }

  openModal = () => {
    this.setState({
        isOpen: true,
    })
  }

  closeModal = () => {
      this.setState({
          isOpen: false,
      })
  }


  render() {

    return(
        <>
        <div className={styles.buttonsWrapper}>
          <Button text='Open first modal' 
          onClick={ event => {
            this.setState(current => ({...current, isFirstOpen: !current.isFirstOpen}));
            event.target.blur();
          }}
          backgroundColor="red" />

          <Button text='Open second modal' 
          onClick={ event => {
            this.setState(current => ({...current, isSecondOpen: !current.isSecondOpen}));
            event.target.blur();
          }}
          backgroundColor="green" />
        </div>

          {this.state.isFirstOpen && <Modal header='Do you want to delete this file?' 
          closeButton={false}
          text={`Once you delete this file, it won't be possible to undo this action.
           Are you shure you want to delete it?`}
          actions={<>
              <Button text='Ok'
              onClick={
                (e) => {
                  if (e.target === e.currentTarget)
                  this.setState(current => ({...current, isFirstOpen: !current.isFirstOpen}))}
              }
             /> 
             <Button
             text='Cancel'
             onClick={
              (e) => {
                if (e.target === e.currentTarget)
                this.setState(current => ({...current, isFirstOpen: !current.isFirstOpen}))}
             }
              /> 
            </>}
          closeModal={
            (e) => {
              if (e.target === e.currentTarget)
              this.setState(current => ({...current, isFirstOpen: !current.isFirstOpen}))}
          } />}

          {this.state.isSecondOpen && <Modal header='Plan B' 
          closeButton={true}
          text={`This text show React's props functionality and have no other sense`}
          actions={<>
              <Button text='Understood'isSecondOpen
              onClick={
                (e) => {
                  if (e.target === e.currentTarget)
                  this.setState(current => ({...current, isSecondOpen: !current.isSecondOpen}))}
              }
             /> 
             <Button
             text='Eazy'
             onClick={
              (e) => {
                if (e.target === e.currentTarget)
                this.setState(current => ({...current, isSecondOpen: !current.isSecondOpen}))}
             }
              /> 
            </>}
          closeModal={(e) => {
            if (e.target === e.currentTarget)
            this.setState(current => ({...current, isSecondOpen: !current.isSecondOpen}))}
            } />}
        </>
    )
  }
}

export default App;