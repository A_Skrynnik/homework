import React, { Component } from 'react';
import styles from './Button.module.scss';

class Button extends Component {
    render() {
        const { backgroundColor, text, onClick } = this.props;
        
        return (
            <button className={styles.btn}
            onClick={ onClick } 
            style={{ backgroundColor, }
            }>{ text }</button>
        )
    }
}

export default Button;