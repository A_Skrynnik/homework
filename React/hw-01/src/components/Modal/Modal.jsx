import React, { Component } from "react";
import styles from './Modal.module.scss'

class Modal extends Component {
    render() {
        const { header, closeButton, text, actions, closeModal } = this.props;

        return (
        <div onClick={ closeModal } className={ styles.background }>
            <div className={ styles.modal }>
                <h3 className={ styles.modalHeader }>
                    { header }{ closeButton && <button onClick={ closeModal }>&#10006;</button> }
                </h3>
                <div className={ styles.modalContent }>
                    <p className={ styles.modalContentText }>
                        { text }
                    </p>
                </div>
                <div className={ styles.modalButtonContainer }>{ actions }</div>
            </div>
        </div>
        )
    }
}

export default Modal;