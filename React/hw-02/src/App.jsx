import { Component } from "react";
import styles from './App.module.scss';
import Header from './components/Header';
import PostsContainer from "./components/PostsContainer/PostsContainer";

class App extends Component {

  state = {
    posts: null,
    isLoading: true,
    isError: false,
  }

  async componentDidMount() {

    try {
      const response = await fetch('./goods.json').then(res => res.json());

      this.setState({
        posts: response,
        isLoading: false,
      })
    } catch(e) {
      this.setState({
        isError: true
      })
    }

  }

  
  render() {
    return (
      <div className={ styles.container }>
      <Header />
      <PostsContainer posts={ this.state.posts } 
      isLoading={ this.state.isLoading } 
      isError={ this.state.isError }
      />
    </div>
    )
  }

}

export default App;