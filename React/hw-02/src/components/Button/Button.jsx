import React, { Component } from 'react';
import styles from './Button.module.scss';
import PropTypes from 'prop-types';

class Button extends Component {
    render() {
        const { backgroundColor, text, onClick } = this.props;
        
        return (
            <button className={ styles.btn }
            onClick={ onClick } 
            style={{ backgroundColor, }
            }>{ text }</button>
        )
    }
};

Button.propTypes = {
    backgroundColor: PropTypes.string,
    text: PropTypes.string,
    onClick: PropTypes.func.isRequired,
};

Button.defaultProps = {
    backgroundColor: 'white',
    text: 'Click me!', 
}

export default Button;