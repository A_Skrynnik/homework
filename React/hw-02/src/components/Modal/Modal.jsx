import { Component } from "react";
import styles from './Modal.module.scss';
import PropTypes from 'prop-types';

class Modal extends Component {
    render() {
        const { header, actions, closeModal } = this.props;

        return (
        <div onClick={ closeModal } className={ styles.background }>
            <div className={ styles.modal }>
                <h3 className={ styles.modalHeader }>
                    { header }
                </h3>
                <div className={ styles.modalButtonContainer }>{ actions }</div>
            </div>
        </div>
        )
    }
}

Modal.propTypes = {
    header: PropTypes.string,
    actions: PropTypes.element,
    closeModal: PropTypes.func,
};

Modal.defaultProps = {
    header: '',
    actions: <></>,
    closeModal: () => {},
}

export default Modal;