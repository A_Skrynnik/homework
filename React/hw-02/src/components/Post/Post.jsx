import { PureComponent } from 'react';
import PropTypes from 'prop-types';
import styles from './Post.module.scss';
import Button from '../Button'
import Modal from '../Modal';

class Post extends PureComponent {

    state={
        inCart: false,
        isFavorite: false,
        isModalOpen: false, 
    }

    render() {
        const { name, price, image, vendorCode, color, id } = this.props.item;

        return (
            <div className={ styles.post }>
                <span>Vendoe code: { vendorCode }</span>
                <div>
                    <img src={ image } alt={ name } width='300' height='300' />
                </div>
                <h3 className={ styles.postHeadlight }>{ name }</h3>
                <p>{ color }</p>
                <p>{ price }</p>
                <div className={ styles.actionBtnsWrapper }>
                    { 
                        !localStorage.getItem(`inCart${id}`) && 
                        <Button text='Add to cart' onClick={() => {
                            this.setState({
                                isModalOpen: true,
                            })
                        }} /> 
                    }


                    { 
                        localStorage.getItem(`inCart${id}`) && 
                        <Button text='Remove from cart' onClick={() => {
                            this.setState( current => ({
                                inCart: !current.inCart
                            }) );
                            localStorage.removeItem(`inCart${id}`);
                        }} /> 
                    }
                    
                    {
                        !localStorage.getItem(`post${id}`) ? 
                        <svg onClick={() => {
                            localStorage.setItem(`post${id}`, 'favorite');
                            this.setState( current => ({
                                isFavorite: !current.isFavorite
                            }))
                        }} className={ styles.addToFavoriteBtn } xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 9.229c.234-1.12 1.547-6.229 5.382-6.229 2.22 0 4.618 1.551 4.618 5.003 0 3.907-3.627 8.47-10 12.629-6.373-4.159-10-8.722-10-12.629 0-3.484 2.369-5.005 4.577-5.005 3.923 0 5.145 5.126 5.423 6.231zm-12-1.226c0 4.068 3.06 9.481 12 14.997 8.94-5.516 12-10.929 12-14.997 0-7.962-9.648-9.028-12-3.737-2.338-5.262-12-4.27-12 3.737z"/></svg>
                        : <svg onClick={() => {
                            localStorage.removeItem(`post${id}`);
                            this.setState( current => ({
                                isFavorite: !current.isFavorite
                            }))
                        }} className={ styles.addToFavoriteBtn } xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 4.435c-1.989-5.399-12-4.597-12 3.568 0 4.068 3.06 9.481 12 14.997 8.94-5.516 12-10.929 12-14.997 0-8.118-10-8.999-12-3.568z"/></svg>
                    }

                    { 
                    this.state.isModalOpen && <Modal header='Add this item to cart?'
                        actions={<>
                            <Button text = "Add"
                            onClick={() => {
                                this.setState( current => ({
                                    inCart: !current.inCart,
                                    isModalOpen: false
                                }) );
                                localStorage.setItem(`inCart${id}`, 'inCart');
                            }} />
                            <Button text = "Cancel"
                            onClick={() => {
                                this.setState({
                                    isModalOpen: false,
                                })
                            }} />
                            </>}
                        closeModal={this.closeModal} /> 
                        }
                </div>
            </div>
        )
    }
};

Post.propTypes = {
    name: PropTypes.string,
    price: PropTypes.number,
    image: PropTypes.string,
    vendorCode: PropTypes.number,
    color: PropTypes.string,
    btnFunc: PropTypes.func,
};

Post.defaultProps ={
    name: '',
    price: 0,
    image: '',
    vendorCode: 0,
    color: '',
    btnFunc: () => {}
}

export default Post;