import { PureComponent  } from "react";
import styles from "./PostsContainer.module.scss"
import PropTypes from 'prop-types';
import Post from "../Post";

class PostsContainer extends PureComponent  {
    render(){
        const { posts, isLoading, isError } = this.props;

        return (
              <section>
                <h3 className={styles.postsHeadlight}>Our best models</h3>
                <div className={styles.postsContainer}>
                    {!isLoading ? posts.map((post) => <Post key={post.id} item={post} />) :
                    'Loadind...'}
                    {isError && <h1>Error!</h1>}
                </div>
            </section>
        );
    }

};

PostsContainer.propTypes = {
    posts: PropTypes.array,
    isLoading: PropTypes.bool,
    isError: PropTypes.bool,
};

PostsContainer.defaultProps = {
    posts: [],
    isLoading: false,
    isError: false,
}

export default PostsContainer;