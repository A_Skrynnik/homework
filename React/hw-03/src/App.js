import { useEffect, useState } from 'react';
import styles from './App.module.scss';
import Modal from './components/Modal';
import Button from './components/Button';
import Header from './components/Header';
import Routes from './Routes/Routes';


const App = () => {
  const [posts, setPosts] = useState([]);
  const [cartItems, setCartItems] = useState([]);
  const [currentItem, setCurrentItem] = useState({});
  const [isLoading, setIsLoading] = useState(true);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isError, setIsError] = useState(false);
  const [isModalForDelete, setIsModalForDelete] = useState(false);


  useEffect(() => {
    try{
      (async () => {
        const data = await fetch('./goods.json').then(res => res.json());
        setPosts(data);
        setIsLoading(false);
      })()
    } catch(e) {
      console.error('Failed to fetch data', e);
      setIsError(true);
    }
  }, []);
  
  const addToCart = () => {
    let index = -1;
    cartItems.forEach(item => {
      if (item.name === currentItem.name) {
        index = cartItems.indexOf(item);
      }
    });
    if (index === -1) {
        setCartItems((current) => [...current, {...currentItem, count: 1}]);
    } else {
        setCartItems((current) => {
            const newState = [...current];
            newState[index].count = newState[index].count + 1;
            return newState;
        });
    };
  };

  const removeFromCart = () => {
    const index = cartItems.indexOf(currentItem);
    const newState = cartItems.slice(0, index).concat(cartItems.slice(index+1));
    setCartItems(newState);
  }

  const toggleModal = () => {setIsModalOpen((current) => !current)};

  const toggleFav = (name) => {
    const index = posts.findIndex(({name: arrayName}) => name === arrayName);
    setPosts((current) => {
      const newState = [...current];
      newState[index].isFavourite = !newState[index].isFavourite;
      return newState;
    });
  };

  return (
    <>
    <div className={styles.container}>
      <Header />
      <Routes 
        posts={posts}
        isLoading={isLoading}
        isError={isError}
        addToCart={addToCart}
        setCurrentItem={setCurrentItem}
        toggleModal={toggleModal}
        toggleFav={toggleFav}
        cartItems={cartItems}
        setIsModalForDelete={setIsModalForDelete}
      />
    </div>

    {isModalOpen && <Modal 
      header="Are you shure?" 
      actions={
          <>
          <Button 
            text={isModalForDelete ? "Remove from cart?" : "Add to cart"}
            onClick={() => {
              isModalForDelete ? removeFromCart() : addToCart();
                toggleModal();
                }} />

          <Button 
            text="Cancel"
            onClick={() => toggleModal()} />
            </>
          } 
          closeModal={() => toggleModal()}
      />}
      </>
  );
};

export default App;