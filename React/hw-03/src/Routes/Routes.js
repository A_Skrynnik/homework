import { Switch, Route } from 'react-router-dom';
import HomePage from '../pages/Home/HomePage';
import CartPage from '../pages/Cart/CartPage';
import FavouritesPage from '../pages/Favourites/FavouritesPage';
import PropTypes  from 'prop-types';

const Routes = (props) => {
    const { posts, isLoading, isError, addToCart, setCurrentItem, toggleModal, toggleFav, cartItems, setIsModalForDelete } = props

    return (
        <Switch>
            <Route exact path='/cart-page'>
                <CartPage
                    cartItems={cartItems}
                    isLoading={isLoading}
                    isError={isError}
                    setCurrentItem={setCurrentItem}
                    toggleModal={toggleModal}
                    toggleFav={toggleFav}
                    setIsModalForDelete={setIsModalForDelete}
                />
            </Route>

            <Route exact path='/favourites'>
                <FavouritesPage
                    posts={posts}
                    isLoading={isLoading}
                    isError={isError}
                    toggleModal={toggleModal}
                    toggleFav={toggleFav}
                    setCurrentItem={setCurrentItem}
                    setIsModalForDelete={setIsModalForDelete}
                />
            </Route>

            <Route exact path='/'>
                <HomePage
                    posts={posts}
                    isLoading={isLoading}
                    isError={isError}
                    addToCart={addToCart}
                    setCurrentItem={setCurrentItem}
                    toggleModal={toggleModal}
                    toggleFav={toggleFav} 
                    setIsModalForDelete={setIsModalForDelete}
                />
            </Route>
        </Switch>
    )
};

Routes.propTypes = {
    posts: PropTypes.array.isRequired,
    cartItems: PropTypes.array.isRequired,
    isLoading: PropTypes.bool,
    isError: PropTypes.bool,
    addToCart: PropTypes.func,
    setCurrentItem: PropTypes.func,
    toggleModal: PropTypes.func,
    toggleFav: PropTypes.func,
    setIsModalForDelete: PropTypes.func,
};

Routes.defaultProps = {
    isLoading: false,
    isError: false,
    addToCart: () => {},
    setCurrentItem: () => {},
    toggleModal: () => {},
    toggleFav: () => {},
    cartItems: () => {},
    setIsModalForDelete: () => {},
}

export default Routes;