import React from 'react';
import styles from './Modal.module.scss';
import PropTypes from 'prop-types';

const Modal = (props) => {
    const { header, actions, closeModal } = props;
    
    return (
    <div onClick={(e) => {e.target === e.currentTarget && closeModal()}} 
        className={styles.background}>
        <div className={styles.modal}>
            <h3 className={styles.modalHeader}>
                {header}
            </h3>
            <div className={styles.modalButtonContainer}>{actions}</div>
        </div>
    </div>
    )
};

Modal.propTypes = {
    header: PropTypes.string,
    actions: PropTypes.element.isRequired,
    closeModal: PropTypes.func.isRequired,
};

Modal.defaultProps = {
    header: '',
}

export default Modal;