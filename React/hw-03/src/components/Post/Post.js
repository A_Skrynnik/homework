import React from "react";
import PropTypes from 'prop-types';
import styles from "./Post.module.scss";
import Button from "../Button";
import { ReactComponent as FavoriteIcon } from '../../assets/svg/heart-thin.svg';
import { ReactComponent as FavoriteIconFilled } from '../../assets/svg/heart-filled.svg';

const Post = (props) => {
    const { post, toggleModal, setCurrentItem, toggleFav, isInCart, setIsModalForDelete } = props;
    const { name, price, image, vendorCode, color, isFavourite, count } = post;
    const deleteFromCart = (post) => {
        setIsModalForDelete(true);
        setCurrentItem(post);
        toggleModal();
    }

    return (
        <div className={styles.post}>
        <span>Vendoe code: {vendorCode}</span>
        {isInCart && <div onClick={() => deleteFromCart(post)} className={styles.deleteButton}>&#10005;</div>}
        <div>
            <img src={image} alt={name} width='300' height='300' />
        </div>
        <h3 className={styles.postHeadlight}>{name}</h3>
        <p>Color: {color}</p>
        <p>Price: {price}</p>
        <div className={styles.postItemButtonsWrapper}>
            {isInCart ? 
            <div style={{fontWeight: 'bold'}}>Quantity: {count}</div> :
                <Button text='Add to cart' onClick={() => {
                    setIsModalForDelete(false);
                    toggleModal();
                    setCurrentItem(post);
                    }} 
                />}
                {!isInCart && <div onClick={() => toggleFav(name)}>
                    {isFavourite ?
                    <FavoriteIconFilled className={styles.addToFavoriteBtn} /> :
                    <FavoriteIcon className={styles.addToFavoriteBtn} />}
                </div>}
            </div>
        </div>
    );
};

Post.propTypes = {
    post: PropTypes.object.isRequired,
    toggleModal: PropTypes.func,
    setCurrentProductName: PropTypes.func,
    toggleFav: PropTypes.func,
    setIsModalForDelete: PropTypes.func,
    isInCart: PropTypes.bool,
};

Post.defaultProps = {
    toggleModal: () => {},
    setCurrentProductName: () => {},
    toggleFav: () => {},
    setIsModalForDelete: () => {},
    isInCart: false,
}

export default Post;