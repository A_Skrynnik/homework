import React from "react";
import Post from "../Post";
import PropTypes from 'prop-types';
import styles from "./PostsContainer.module.scss";

const PostsContainer = (props) => {
    const { posts, isLoading, isError, toggleModal, setCurrentItem, toggleFav, isInCart, setIsModalForDelete } = props;
    
    return (
        <section>
            <div className={styles.postsContainer}>
              {
              (!isLoading && !isError) ? 
              posts.map((post) => (
                <Post 
                key={post.id} 
                post={post}
                toggleModal={toggleModal} 
                setCurrentItem={setCurrentItem}
                toggleFav={toggleFav}
                isInCart={isInCart}
                setIsModalForDelete={setIsModalForDelete}
                />)
              ) : 
              'Loadind...'
              }
              {isError && <h1>Error, failed to fetch data! Look into the console for more detailes</h1>}
          </div>
      </section>
  );
};

PostsContainer.propTypes = {
  posts: PropTypes.array.isRequired,
  isError: PropTypes.bool,
  isLoading: PropTypes.bool,
  isInCart: PropTypes.bool,
  toggleModal: PropTypes.func,
  setCurrentProductName: PropTypes.func,
  toggleFav: PropTypes.func,
  setIsModalForDelete: PropTypes.func,
};

PostsContainer.defaultProps = {
  isError: false,
  isLoading: false,
  toggleModal: () => {},
  setCurrentProductName: () => {},
  toggleFav: () => {},
  toggleFav: () => {},
  setIsModalForDelete: () => {},
}

export default PostsContainer;