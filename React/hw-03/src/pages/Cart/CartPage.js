import PostsContainer from "../../components/PostsContainer/PostsContainer";
import PropTypes  from 'prop-types';

const CartPage = (props) => {
    const { cartItems, isLoading, isError, toggleModal, toggleFav, setCurrentItem, setIsModalForDelete } = props;

    
    
    return (
        cartItems.length > 0 ?
        <PostsContainer
            posts={cartItems}
            isLoading={isLoading}
            isError={isError}
            toggleModal={toggleModal}
            toggleFav={toggleFav} 
            setCurrentItem={setCurrentItem}
            isInCart={true}
            setIsModalForDelete={setIsModalForDelete}
        /> :
        <div style={{display: 'flex', justifyContent: 'center', marginTop: '30vh'}}><h3>Cart is empty :(</h3></div>
    );
};

CartPage.propTypes = {
    cartItems: PropTypes.array.isRequired,
    isLoading: PropTypes.bool,
    isError: PropTypes.bool,
    toggleModal: PropTypes.func,
    toggleFav: PropTypes.func,
    setCurrentItem: PropTypes.func,
    setIsModalForDelete: PropTypes.func,
};

CartPage.defaultProps = {
    isLoading: false,
    isError: false,
    toggleModal: () => {},
    toggleFav: () => {},
    setCurrentItem: () => {},
    setIsModalForDelete: () => {},
}

export default CartPage;