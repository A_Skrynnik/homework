import PostsContainer from "../../components/PostsContainer/PostsContainer";
import PropTypes  from 'prop-types';

const FavouritesPage = (props) => {
    const { posts, isLoading, isError, toggleModal, toggleFav, setCurrentItem, setIsModalForDelete } = props;
    const favourites = posts.filter(post => post.isFavourite === true);

    return (
        favourites.length > 0 ?
        <PostsContainer
            posts={favourites}
            isLoading={isLoading}
            isError={isError}
            toggleModal={toggleModal}
            toggleFav={toggleFav} 
            setCurrentItem={setCurrentItem}
            setIsModalForDelete={setIsModalForDelete}
        /> :
        <div style={{display: 'flex', justifyContent: 'center', marginTop: '30vh'}}><h3>You don't like anything yet :(</h3></div>
    );
};

FavouritesPage.propTypes = {
    posts: PropTypes.array.isRequired,
    isLoading: PropTypes.bool,
    isError: PropTypes.bool,
    toggleModal: PropTypes.func,
    toggleFav: PropTypes.func,
    setCurrentItem: PropTypes.func,
    setIsModalForDelete: PropTypes.func,
};

FavouritesPage.defaultProps = {
    isLoading: false,
    isError: false,
    toggleModal: () => {},
    toggleFav: () => {},
    setCurrentItem: () => {},
    setIsModalForDelete: () => {},
}

export default FavouritesPage;