import PostsContainer from "../../components/PostsContainer/PostsContainer";
import PropTypes  from 'prop-types';

const HomePage = (props) => {
    const { posts, isLoading, isError, addToCart, setCurrentItem, toggleModal, toggleFav, setIsModalForDelete } = props

    return (<PostsContainer
    posts={posts}
    isLoading={isLoading}
    isError={isError}
    addToCart={addToCart}
    setCurrentItem={setCurrentItem}
    toggleModal={toggleModal}
    toggleFav={toggleFav}
    setIsModalForDelete={setIsModalForDelete} />)
};

HomePage.propTypes = {
    posts: PropTypes.array.isRequired,
    isLoading: PropTypes.bool,
    isError: PropTypes.bool,
    addToCart: PropTypes.func,
    setCurrentItem: PropTypes.func,
    toggleModal: PropTypes.func,
    toggleFav: PropTypes.func,
    setIsModalForDelete: PropTypes.func,
};

HomePage.defaulProps = {
    isLoading: false,
    isError: false,
    addToCart: () => {},
    setCurrentItem: () => {},
    toggleModal: () => {},
    toggleFav: () => {},
    setIsModalForDelete: () => {},
}

export default HomePage;