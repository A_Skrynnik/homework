import { BrowserRouter as Router } from 'react-router-dom';
import { useSelector } from 'react-redux';
import styles from './App.module.scss';
import Modal from './components/Modal';
import Header from './components/Header';
import Routes from './Routes/Routes';



const App = () => {
  const { isModalOpen } = useSelector(store => store.modal);

  return (
    <Router>
      <div className={styles.container}>
        <Header />
        <Routes />
      </div>

      {isModalOpen && <Modal header='Are you shure?'/>}
    </Router>
  );
};

export default App;