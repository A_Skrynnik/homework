import React from "react";
import styles from "./Button.module.scss";
import PropTypes from 'prop-types';

const Button = (props) => {
    const { text, onClick } = props;

    return (
        <button 
        className={styles.btn}
        onClick={onClick}>
            {text}
        </button>
    )
};

Button.propTypes = {
    text: PropTypes.string,
    onClick: PropTypes.func,
};

Button.defaultProps = {
    text: '',
    onClick: () => {},
};
export default Button;