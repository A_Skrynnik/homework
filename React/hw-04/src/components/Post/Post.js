import React from "react";
import PropTypes from 'prop-types';
import styles from "./Post.module.scss";
import Button from "../Button";
import { ReactComponent as FavoriteIcon } from '../../assets/svg/heart-thin.svg';
import { ReactComponent as FavoriteIconFilled } from '../../assets/svg/heart-filled.svg';
import { toggleIsFavouriteCards } from "../../store/actionCreators/postsActionCreators";
import { useDispatch } from "react-redux";
import { setIsModalOpen } from '../../store/actionCreators/modalActionCreators';
import { setIsModalForDelete } from "../../store/actionCreators/modalActionCreators";
import { setCurrentItem } from "../../store/actionCreators/postsActionCreators";


const Post = (props) => {
    const { post, isInCart } = props;
    const { name, price, image, vendorCode, color, isFavourite, count } = post;
    
    const dispatch = useDispatch()
    
    const deleteFromCart = (post) => {
        dispatch(setIsModalForDelete(true));
        dispatch(setCurrentItem(post));
        dispatch(setIsModalOpen(true));
    };
    return (
        <div className={styles.post}>
        <span>Vendoe code: {vendorCode}</span>
        {isInCart && <div onClick={() => deleteFromCart(post)} className={styles.deleteButton}>&#10005;</div>}
        <div>
            <img src={image} alt={name} width='300' height='300' />
        </div>
        <h3 className={styles.postHeadlight}>{name}</h3>
        <p>Color: {color}</p>
        <p>Price: {price}</p>
        <div className={styles.postItemButtonsWrapper}>
            {isInCart ? 
            <div style={{fontWeight: 'bold'}}>Quantity: {count}</div> :
                <Button text='Add to cart' onClick={() => {
                    dispatch(setIsModalForDelete(false));
                    dispatch(setIsModalOpen(true))
                    dispatch(setCurrentItem(post));
                    }} 
                />}
                {!isInCart && <div onClick={() => dispatch(toggleIsFavouriteCards(post))}>
                    {isFavourite ?
                    <FavoriteIconFilled className={styles.addToFavoriteBtn} /> :
                    <FavoriteIcon className={styles.addToFavoriteBtn} />}
                </div>}
            </div>
        </div>
    );
};

Post.propTypes = {
    post: PropTypes.object.isRequired,
    isInCart: PropTypes.bool,
};

Post.defaultProps = {
    isInCart: false,
}

export default Post;