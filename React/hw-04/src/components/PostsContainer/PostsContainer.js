import React from "react";
import Post from "../Post";
import PropTypes from 'prop-types';
import styles from "./PostsContainer.module.scss";
import { useSelector } from "react-redux";

const PostsContainer = (props) => {
    const { posts, isInCart } = props;
    const { isLoading, isError } = useSelector(state => state.posts)

    return (
        <section>
            <div className={styles.postsContainer}>
              {
              (!isLoading && !isError) ? 
              posts.map((post) => (
                <Post 
                key={post.id} 
                post={post}
                isInCart={isInCart}
                />)
              ) : 
              'Loading...'
              }
              {isError && <h1>Error, failed to fetch data! Look into the console for more detailes</h1>}
          </div>
      </section>
  );
};

PostsContainer.propTypes = {
  posts: PropTypes.array.isRequired,
  isInCart: PropTypes.bool,
};

PostsContainer.defaultProps = {
  isInCart: false,
}

export default PostsContainer;