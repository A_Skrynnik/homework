import { createStore, applyMiddleware } from "redux";
import {composeWithDevTools} from "redux-devtools-extension";
import thunk from 'redux-thunk';
import reducers from "./reducers";

const appStore = createStore(reducers, composeWithDevTools(applyMiddleware(thunk)));

export default appStore;