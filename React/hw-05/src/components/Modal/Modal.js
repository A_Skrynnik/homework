import React from 'react';
import styles from './Modal.module.scss';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { setIsModalOpen } from '../../store/actionCreators/modalActionCreators';
import { addToCart, removeFromCart } from '../../store/actionCreators/cartActionCreators';
import Button from '../Button';

const Modal = ({ header }) => {
    const dispatch = useDispatch();
    const { isModalForDelete } = useSelector(state => state.modal)
    const { currentItem: item } = useSelector(state => state.posts)

    const closeModal = () => dispatch(setIsModalOpen(false));
    
    return (
    <div onClick={(e) => {e.target === e.currentTarget && closeModal()}} 
        className={styles.background}>
        <div className={styles.modal}>
            <h3 className={styles.modalHeader}>
                {header}
            </h3>
            <div className={styles.modalButtonContainer}>{
                <>
                <Button 
                  text={isModalForDelete ? "Remove from cart?" : "Add to cart"}
                  onClick={() => {
                    isModalForDelete ? dispatch(removeFromCart(item)) : dispatch(addToCart(item));
                    closeModal();
                      }} 
                      />
      
                <Button 
                  text="Cancel"
                  onClick={() => closeModal()} />
                  </>
            }</div>
        </div>
    </div>
    )
};

Modal.propTypes = {
    header: PropTypes.string,
};

Modal.defaultProps = {
    header: 'Are you shure?',
}

export default Modal;