import CustomInput from '../CustomInput';
import { Formik, Form } from 'formik';
import * as yup from 'yup'
import { Button } from '@mui/material';
import styles from "./ShippingForm.module.scss";
import { useDispatch, useSelector } from 'react-redux';
import { clearCart } from '../../store/actionCreators/cartActionCreators';

const ShippingForm = () => {
    const dispatch = useDispatch();
    const { cartItems } = useSelector(state => state.cart);
    const initialValues = {
        firstName: '',
        lastName: '',
        age: '',
        shippingAdress: '',
        phone: '',
    };

    const validationSchema = yup.object().shape({
        firstName: yup.string().required('Name is required').min(2).max(25),
        lastName: yup.string().required('Last name is required').min(2).max(25),
        age: yup.number().required('Age is required'),
        shippingAdress: yup.string().required('Adress is required'),
        phone: yup.number().required('Phone numder is required'),
    });

    const handleSubmit = (val, actions) => {
        console.log("Products to order", cartItems);
        console.log("Custumers info ", val);
        actions.resetForm();
        dispatch(clearCart());
        actions.setSubmitting(true);
        setTimeout(()=>{
            actions.setSubmitting(false);
        },2000);
    };

    return (
        <>
        <Formik 
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={handleSubmit}
        >
            {({dirty, isSubmiting}) => (
            <Form className={styles.shippingForm}>
                <CustomInput 
                    name='firstName'
                    type='text'
                    label='First name'
                    placeholder='First name*'
                />
    
                <CustomInput 
                    name='lastName'
                    type='text'
                    label='Last name'
                    placeholder='Last name*'
                />
    
                <CustomInput 
                    name='age'
                    type='text'
                    label='Age'
                    placeholder='Age*'
                />
    
                <CustomInput 
                    name='shippingAdress'
                    type='text'
                    label='Shipping adress'
                    placeholder='Shipping adress*'
                />
    
                <CustomInput 
                    name='phone'
                    type='text'
                    label='Phone numder'
                    placeholder='Phone numder*'
                />

                <Button variant="contained" type='submit' disabled={isSubmiting}>Order</Button>
            </Form>
            )}

        </Formik>
        </>
    );
};

export default ShippingForm;