import PostsContainer from "../../components/PostsContainer/PostsContainer";
import { shallowEqual, useSelector } from "react-redux";

const FavouritesPage = () => {
    const { posts } = useSelector(store => store.posts, shallowEqual);
    const favourites = posts.filter(post => post.isFavourite === true);

    return (
        favourites.length > 0 ?
        <PostsContainer
            posts={favourites}
        /> :
        <div style={{display: 'flex', justifyContent: 'center', marginTop: '30vh'}}><h3>You don't like anything yet :(</h3></div>
    );
};

export default FavouritesPage;