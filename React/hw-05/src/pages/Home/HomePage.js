import PostsContainer from "../../components/PostsContainer/PostsContainer";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { initPosts } from "../../store/actionCreators/postsActionCreators";
import { useEffect } from "react";

const HomePage = () => {
    const dispatch = useDispatch();
    const { posts } = useSelector((store) => store.posts, shallowEqual);

    useEffect(() => {
      dispatch(initPosts())
    }, []);

    return (
    <PostsContainer
        posts={posts}
    />)
};

export default HomePage;