import { SET_IS_MODAL_FOR_DELETE, SET_IS_MODAL_OPEN } from "../actions";

export const setIsModalOpen = isOpen => ({type: SET_IS_MODAL_OPEN, payload: isOpen});
export const setIsModalForDelete = isForDelete => ({type: SET_IS_MODAL_FOR_DELETE, payload: isForDelete});