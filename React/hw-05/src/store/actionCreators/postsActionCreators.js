import { INIT_POSTS, SET_IS_LOADING, SET_IS_ERROR, TOGGLE_IS_FAVOURITE_CARDS, SET_CURRENT_ITEM } from "../actions";

export const setIsLoading = isLoading => ({type: SET_IS_LOADING, payload: isLoading});
export const setIsError = isError => ({type: SET_IS_ERROR, payload: isError});
export const initPosts = () => async(dispatch) => {
    try{
        setIsLoading(true);
        const posts = await fetch('./goods.json').then(res => res.json());
        dispatch({type: INIT_POSTS, payload: posts});
    } catch(e) {
        console.error('Failed to fetch data', e);
        setIsError(true);
    };
    setIsLoading(false);
};
export const toggleIsFavouriteCards = (item) => ({type: TOGGLE_IS_FAVOURITE_CARDS, payload: item});
export const setCurrentItem = item => ({type: SET_CURRENT_ITEM, payload: item})