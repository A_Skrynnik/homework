// Posts actions
export const INIT_POSTS = 'INIT_POSTS';
export const SET_IS_LOADING = 'SET_IS_LOADING';
export const SET_IS_ERROR = 'SET_IS_ERROR';
export const TOGGLE_IS_FAVOURITE_CARDS = 'TOGGLE_IS_FAVOURITE_CARDS';
export const SET_CURRENT_ITEM = 'SET_CURRENT_ITEM'

//Modal actions
export const SET_IS_MODAL_OPEN = 'SET_IS_MODAL_OPEN';
export const SET_IS_MODAL_FOR_DELETE = 'SET_IS_MODAL_FOR_DELETE';

// Cart actions
export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';
export const CLEAR_CART = 'CLEAR_CART';