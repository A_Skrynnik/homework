import { INIT_POSTS, SET_CURRENT_ITEM, SET_IS_ERROR, SET_IS_LOADING, TOGGLE_IS_FAVOURITE_CARDS } from "../actions";

const initialState = {
    posts: [],
    currentItem: {},
    isLoading: false,
    isError: false,
};

const reducer = (state=initialState, {type, payload}) => {
    switch(type) {
        case INIT_POSTS:
            return {...state, posts: payload};
        case SET_IS_LOADING:
            return {...state, isLoading: payload};
        case SET_IS_ERROR:
            return {...state, isError: payload};
        case SET_CURRENT_ITEM:
            return {...state, currentItem: payload};
        case TOGGLE_IS_FAVOURITE_CARDS:
            const newData = [...state.posts];
            const targetIndex = newData.findIndex(e => e.id === payload.id);
            newData[targetIndex] = {...payload, isFavourite: !payload.isFavourite};
            return {...state, posts: newData};
        default:
            return state;
    };
};

export default reducer;