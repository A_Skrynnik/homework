import { BrowserRouter as Router } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import styles from './App.module.scss';
import Modal from './components/Modal';
import Header from './components/Header';
import Routes from './Routes/Routes';
import Button from "./components/Button";
import {addToCart, removeFromCart} from "./store/actionCreators/cartActionCreators";
import React from "react";
import {setIsModalOpen} from "./store/actionCreators/modalActionCreators";


const App = () => {
    const { isModalForDelete } = useSelector(state => state.modal);
    const { currentItem: item } = useSelector(state => state.posts);
    const dispatch = useDispatch();

  return (
    <Router>
      <div className={styles.container}>
        <Header />
        <Routes />
      </div>

      <Modal
        header='Are you sure?'
        actions={
            <Button
                text={isModalForDelete ? "Remove from cart?" : "Add to cart"}
                onClick={() => {
                    isModalForDelete ? dispatch(removeFromCart(item)) : dispatch(addToCart(item));
                    dispatch(setIsModalOpen(false));
                }}
            />
        }
      />
    </Router>
  );
};

export default App;