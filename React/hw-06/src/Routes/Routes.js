import { Switch, Route } from 'react-router-dom';
import HomePage from '../pages/Home/HomePage';
import CartPage from '../pages/Cart/CartPage';
import FavouritesPage from '../pages/Favourites/FavouritesPage';

const Routes = () => {

    return (
        <Switch>
            <Route exact path='/cart-page'>
                <CartPage/>
            </Route>

            <Route exact path='/favourites'>
                <FavouritesPage/>
            </Route>

            <Route exact path='/'>
                <HomePage/>
            </Route>
        </Switch>
    )
};

export default Routes;