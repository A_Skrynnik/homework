import { render } from '@testing-library/react'
import Button from "./Button";

const handleClick = jest.fn();

describe('Button render snapshot test', () => {
    it("Should Button without props matches snapshot", () => {
        const { asFragment } = render(<Button />);
        expect(asFragment()).toMatchSnapshot();
    });
    it("Should Button with props matches snapshot", () => {
        const { asFragment } = render(<Button text='Click me' />);
        expect(asFragment()).toMatchSnapshot();
    });
});

describe("Button onClick function test", () => {
    it('Should Button onClick function works', () => {
        const { getByText } = render(<Button text='Click me' onClick={handleClick} />);
        const button = getByText('Click me');
        button.click();
        expect(handleClick).toBeCalled();
    });
})