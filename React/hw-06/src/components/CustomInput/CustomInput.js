import { useField } from 'formik';
import { TextField } from "@mui/material";
import { PropTypes } from 'prop-types';
import styles from './CustomInput.module.scss'

const CustomInput = props => {
    const [field, meta] = useField(props);
    const {label, type, placeholder} = props;
    const isError = meta.touched && meta.error;

    return (
        <>
        <TextField 
            {...field}
            label={label}
            type={type}
            placeholder={placeholder}
            color={isError ? 'error' : 'primary'}
        />
        <span className={styles.error}>{isError ? meta.error : ''}</span>
        </>
    );
};

CustomInput.propTypes = {
    type: PropTypes.string,
    placeholder: PropTypes.string,
};
CustomInput.defaultProps = {
    type: 'text',
    placeholder: '',
};

export default CustomInput;