import React from "react";
import styles from "./Header.module.scss";
import { ReactComponent as CartIcon } from '../../assets/svg/shopping-cart.svg';
import { ReactComponent as FavIcon } from '../../assets/svg/heart-filled.svg'
import { ReactComponent as HomeIcon } from '../../assets/svg/home.svg'
import { Link } from "react-router-dom";

const Header = () => (
    <header className={styles.header}>
        <h1 className={styles.headerText}>
            Best headfones for best prices
        </h1>
        <div className={styles.headerNavigationIconsWrapper}>
        <Link to='/'><HomeIcon className={styles.headerHomeIcon}/></Link>
        <Link to='/favourites'><FavIcon className={styles.headerFavIcon}/></Link>
        <Link to='/cart-page'><CartIcon className={styles.headerCartIcon}/></Link>
        </div>
    </header>
);

export default Header;