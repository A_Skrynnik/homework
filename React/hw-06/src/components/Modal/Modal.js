import React from 'react';
import styles from './Modal.module.scss';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { setIsModalOpen } from '../../store/actionCreators/modalActionCreators';
import Button from '../Button';

const Modal = ({ header, actions }) => {
    const dispatch = useDispatch();
    const { isModalOpen } = useSelector(store => store.modal);
    const closeModal = () => dispatch(setIsModalOpen(false));

    if(!isModalOpen) return null

    return (
    <div data-testid='modal_test' onClick={(e) => {e.target === e.currentTarget && closeModal()}}
        className={styles.background}>
        <div className={styles.modal}>
            <h3 className={styles.modalHeader}>
                {header}
            </h3>
            <div className={styles.modalButtonContainer}>{
                <>
                {actions}
                <Button 
                  text="Cancel"
                  onClick={() => closeModal()} />
                  </>
            }</div>
        </div>
    </div>
    )
};

Modal.propTypes = {
    header: PropTypes.string,
};

Modal.defaultProps = {
    header: 'Are you sure?',
}

export default Modal;