import Modal from "./Modal";
import { render } from "@testing-library/react";
import { Provider } from "react-redux";
import appStore from "../../store/appStore";
import { setIsModalOpen } from "../../store/actionCreators/modalActionCreators";
import { useDispatch } from "react-redux";
import Button from "../Button";
import React from "react";
import '@testing-library/jest-dom'

const addToCart = jest.fn();

const Component = () => {
    const dispatch = useDispatch();

    return (
        <>
            <button onClick={() => dispatch(setIsModalOpen(true))}>Open modal</button>
            <button onClick={() => dispatch(setIsModalOpen(false))}>Close modal</button>
            <Modal
                title='Modal title'
                actions={
                    <Button
                        text='add to cart'
                        onClick={addToCart}
                    />
                }
            />
        </>
    );
};


describe('Modal snapshot test', () => {
    test('Should Modal render without props', () => {
        const { asFragment } = render(<Provider store={appStore}><Modal /></Provider>);
        expect(asFragment()).toMatchSnapshot();
    });
    test('Should Modal render with header and actions props', () => {
        const { asFragment } = render(<Provider store={appStore}><Component /></Provider>);
        expect(asFragment()).toMatchSnapshot();
    });
});

describe('Test is Modal opening and closing when should', () => {
    it('Is modal close when open-modal button was NOT clicked', () => {
        const { queryByTestId } = render(<Provider store={appStore}><Component /></Provider>);
        expect(queryByTestId('modal_test')).not.toBeInTheDocument();
    });
    it('Is modal open when open-modal button was clicked', () => {
        const { queryByTestId, getByText } = render(<Provider store={appStore}><Component /></Provider>);
        const openButton = getByText('Open modal');
        openButton.click();
        expect(queryByTestId('modal_test')).toBeInTheDocument();
    });
    it('Is modal close when close-modal button button was clicked', () => {
        const { queryByTestId, getByText } = render(<Provider store={appStore}><Component /></Provider>);
        const closeButton = getByText('Cancel');
        closeButton.click();
        expect(queryByTestId('modal_test')).not.toBeInTheDocument();
    });
});


