import PostsContainer from "../../components/PostsContainer/PostsContainer";
import { useSelector } from "react-redux";

const CartPage = () => {
    const { cartItems } = useSelector(state => state.cart);
    
    
    return (
        cartItems.length > 0 ?
        <PostsContainer
            posts={cartItems}
            isInCart={true}
        /> :
        <div style={{display: 'flex', justifyContent: 'center', marginTop: '30vh'}}><h3>Cart is empty :(</h3></div>
    );
};

export default CartPage;