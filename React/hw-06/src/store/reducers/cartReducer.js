import { ADD_TO_CART, CLEAR_CART, REMOVE_FROM_CART } from "../actions";

const initialState = {
    cartItems: [],
};

const cartReducer = (state=initialState, {type, payload}) => {
    switch(type) {
        case ADD_TO_CART:
            let index = -1;
            state.cartItems.forEach(item => {
                if (item.name === payload.name) {
                  index = state.cartItems.indexOf(item);
                };
            });
            if (index === -1) {
                return {...state, cartItems: [...state.cartItems, {...payload, count: 1}]};
            } else {
                const newState = {...state};
                newState.cartItems[index].count = ++newState.cartItems[index].count; 
                return newState
            };
        case REMOVE_FROM_CART:
            const removeIndex = state.cartItems.indexOf(payload);
            const newState = state.cartItems.slice(0, removeIndex).concat(state.cartItems.slice(removeIndex+1));
            return {...state, cartItems: newState};
        case CLEAR_CART:
            return {...state, cartItems: []};
        default:
            return state;
    };
};

export default cartReducer;