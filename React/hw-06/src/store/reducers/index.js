import { combineReducers } from 'redux';
import postsReducer from './postsReducer';
import modalReducer from './modalReducer';
import cartReducer from './cartReducer';

const reducers = combineReducers({
    posts: postsReducer,
    modal: modalReducer,
    cart: cartReducer,
});

export default reducers;