import { SET_IS_MODAL_FOR_DELETE, SET_IS_MODAL_OPEN } from "../actions";

const initialState = {
    isModalOpen: false,
    isModalForDelete: false,
};

const modalReducer = (state=initialState, {type, payload}) => {
    switch(type) {
        case SET_IS_MODAL_OPEN:
            return {...state, isModalOpen: payload};
        case SET_IS_MODAL_FOR_DELETE:
            return {...state, isModalForDelete: payload};
        default:
            return state;
    };
};

export default modalReducer;