const bookList = {
    books: [
        { 
          author: "Скотт Бэккер",
          name: "Тьма, что приходит прежде",
          price: 70 
        }, 
        {
         author: "Скотт Бэккер",
         name: "Воин-пророк",
        }, 
        { 
          name: "Тысячекратная мысль",
          price: 70
        }, 
        { 
          author: "Скотт Бэккер",
          name: "Нечестивый Консульт",
          price: 70
        }, 
        {
         author: "Дарья Донцова",
         name: "Детектив на диете",
         price: 40
        },
        {
         author: "Дарья Донцова",
         name: "Дед Снегур и Морозочка",
        }
      ],
    container: document.querySelector('#root'),
    list: document.createElement('ul'),
    render() {
        this.books.forEach( el => {
            try {
                if (!el.author) {
                    throw new ReferenceError('Missing author property');
                } else if (!el.name) {
                    throw new ReferenceError('Missing name property');
                } else if (!el.price) {
                    throw new ReferenceError('Missing price property');
                } else {
                    this.list.insertAdjacentHTML('beforeend',
                    `<li> ${el.author} "${el.author}", ${el.price} </li>`);
                }
            } catch(e) {
                console.log(`${e.name}: ${e.message}`);
            }
        })
        this.container.append(this.list)
    }
}

bookList.render();