const preloader = `
<span></span>
<span></span>
<span></span>
<span></span>
<span></span>
`
const container = document.querySelector('#film-container');

fetch('https://ajax.test-danit.com/api/swapi/films')
  .then((response) => response.json() )
  .then((data) => {
    data.forEach( ({characters, episodeId, openingCrawl, name}) => {

        container.insertAdjacentHTML('beforeend', `
        <div id="episode${episodeId}">
            <h3 id="episode-name">${name}</h3>
            <span id="episode-number">Episode №${episodeId}</span>
            <span>Actors:</span>
            <div id='episode${episodeId}-preloader' class="loader pending">
              ${preloader}
            </div>
            <ul id="episode${episodeId}-characters"></ul>
            <span id="episode-description">&nbsp;&nbsp;&nbsp;&nbsp;${openingCrawl}</span>
        </div>
        `);


        characters.forEach( url => {
            fetch( url )
            .then( response => response.json() )
            .then( ({ name }) => {

              document.querySelector(`#episode${episodeId}-preloader`)
              .classList.remove('pending');

              document.querySelector(`#episode${episodeId}-characters`)
              .insertAdjacentHTML("beforeend", `<li>${name}</li>`);

            } ) 
        } )

    } )
  });