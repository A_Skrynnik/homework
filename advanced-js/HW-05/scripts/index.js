const showIpInfo = async() => {
    const { data: { ip } } = await axios.get('https://api.ipify.org/?format=json');
    const { data: {continent, country, region, city, district} } = await axios
    .get(`http://ip-api.com/json/${ip}?fields=continent,country,region,city,district`);

    document.querySelector('#get-ip-wrapper').insertAdjacentHTML('beforeend', `
    <div id="ip-about-wrapper">
        <span>Continent: ${continent}</span>
        <span>Country: ${country}</span>
        <span>Region: ${region}</span>
        <span>City: ${city}</span>
        <span>District: ${district}</span>
    </div>
    `)
}

document.querySelector('#get-ip').addEventListener('click', showIpInfo)